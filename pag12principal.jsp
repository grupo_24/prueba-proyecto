<%-- 
    Document   : pag12_principal
    Created on : 31/08/2022, 21:28:45
    Author     : Stranger Max
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>PROBANDO 1,2 ,3
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ejemplo: Acciones "include con parametros"</h1>
        <jsp:forward page="pag12secundaria.jsp">
            <jsp:param name="parNom" value="Max" />
            <jsp:param name="parApe" value="Vargas" />
        </jsp:forward>
    </body>
</html>
